import { useState, useEffect } from 'react';

function getSize(){
	return {
		width: window.innerWidth,
		height: window.innerHeight
	};
}

export default function(){
	let [size, setSize] = useState(getSize());

	useEffect(() => {
		let resizeHandler = function(){
			setSize(getSize());
		}

		window.addEventListener('resize', resizeHandler);

		return () => {
			window.removeEventListener('resize', resizeHandler);
		}
	}, []);
	
	return size;
}