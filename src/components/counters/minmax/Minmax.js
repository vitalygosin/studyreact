import React, { useState, useEffect } from 'react';
import styles from './minmax.module.css';
import PropTypes from 'prop-types';

Minmax.defaultProps = { min: 1 }
Minmax.propTypes = {
	min: PropTypes.number,
	max: PropTypes.number.isRequired,
	current: PropTypes.number.isRequired,
	changed: PropTypes.func
}

function Minmax({ min, max, current, changed }){
	let [inpVal, setInpVal] = useState(current);

	let applyCurrent = (newCur) => {
		let validCurrent = Math.max(Math.min(newCur, max), min);
		setInpVal(validCurrent);
		changed(validCurrent);
	}

	let onInput = (e) => setInpVal(e.target.value);
	let inc = () => applyCurrent(current + 1);
	let dec = () => applyCurrent(current - 1);

	let inpChanged = () => {
		let newCur = parseInt(inpVal);
		applyCurrent(isNaN(newCur) ? min : newCur);
	}

	let checkEnter = e => {
		if(e.keyCode === 13){
			inpChanged();
		}
	}

	useEffect(() => {
		if(inpVal.toString() != current.toString()){
			setInpVal(current);
		}
	}, [current]);
	
	return (
		<div className={styles.alert}>
			<button className={`btn btn-danger`} type="button" onClick={dec} disabled={current <= min}>-</button>
			&nbsp;
			<input type="text" value={inpVal} onChange={onInput} onBlur={inpChanged} onKeyDown={checkEnter} />
			&nbsp;
			<button className={`btn btn-success`} type="button" onClick={inc} disabled={current >= max}>+</button>
		</div>
	);
}

export default Minmax;