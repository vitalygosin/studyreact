import React, { useState } from 'react';

import { BrowserRouter, Switch, Route, NavLink } from "react-router-dom";
import routes, { routesMap } from '../router';

export default function(){
	let routesCmp = routes.map(route => {
		return <Route key={route.path} path={route.path} component={route.component} exact></Route>
	});

	return <BrowserRouter>
		<div className="container mt-3">
			<div className="row">
				<div className="col col-3">
					<ul className="list-group">
						<li className="list-group-item">
							<NavLink to={routesMap.home} activeClassName="text-danger" exact>Products</NavLink>
						</li>
						<li className="list-group-item">
							<NavLink to={routesMap.cart} activeClassName="text-danger">Cart</NavLink>
						</li>
						<li className="list-group-item">
							<NavLink to={routesMap.order} activeClassName="text-danger">Order</NavLink>
						</li>
					</ul>
				</div>
				<div className="col col-9">
					<Switch>
						{routesCmp}
					</Switch>
				</div>
			</div>
			<hr/>
		</div>
	</BrowserRouter>
}