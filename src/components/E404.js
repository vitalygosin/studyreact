import React from 'react';

export default function(){
	return <>
		<h1>Page not found</h1>
		<hr/>
		<div className="alert alert-danger">
			Try restart your computer :) lol
		</div>
	</>
}