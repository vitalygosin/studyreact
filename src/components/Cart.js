import React, { useContext } from 'react';
import CartItem from './CartItem';

import { observer } from 'mobx-react-lite';
import storesContext from './../contexts/stores';

import {Link, Redirect, useHistory} from "react-router-dom";

export default observer(function({ onNext }){
	let { cart: cartStore } = useContext(storesContext);
	let { productsDetailed: products, setCnt: onChange, remove: onRemove } = cartStore;

	let total = products.reduce((sum, pr) => sum + pr.cnt * pr.price, 0);
	let productRows = products.map(pr => (
		<CartItem 
			key={pr.id} 
			{...pr}
			onChange={onChange}
			onRemove={onRemove}
		/>
	));

	return <>
		<h1>Cart!</h1>
		<hr/>
		<table className="table table-bordered">
			<thead>
				<tr>
					<td>Title</td>
					<td>Count</td>
					<td>Price</td>
					<td>Total</td>
					<td>Actions</td>
				</tr>
			</thead>
			<tbody>
				{productRows}
			</tbody>
		</table>
		<hr/>
		<strong>Total: {total}</strong>
		<hr/>
		<button type="button" ><Link to='/checkout'>Send</Link></button>
	</>
});