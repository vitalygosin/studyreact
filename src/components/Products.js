import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import storesContext from '../contexts/stores';
import { Card, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { createPath } from '../router';

export default observer(function(){
	let { 
		products: { items: products }, 
		cart: { add, remove, inCart } 
	} = useContext(storesContext);

	let productsCards = products.map((pr) => {
		let btn = inCart(pr.id) ? 
			<button type="button" className="btn btn-danger" onClick={() => remove(pr.id)}>Remove</button> :
			<button type="button" className="btn btn-success" onClick={() => add(pr.id)}>Add</button>;

		return <Col xs={4} key={pr.id} className="mt-3 mb-3">
			<Card>
				<Card.Body>
					<Card.Title>{pr.title}</Card.Title>
					<Card.Text>
						<strong>Price: {pr.price}</strong>
					</Card.Text>
					<Link to={createPath('product', { id: pr.id })}>
						Get more...
					</Link>
					<hr/>
					{btn}
				</Card.Body>
			</Card>
		</Col>
	});

	return <div>
		<h1>Products List</h1>
		<hr/>
		<Row>{productsCards}</Row>
	</div>
});