import React from 'react';
import ReactDom from 'react-dom';
import App from './components/App';

import stores from './store';
import storesContext from './contexts/stores';


import 'bootstrap/dist/css/bootstrap.css'
import './assets/styles/style.css';

stores.cart.load();
stores.products.load().then(() => {
ReactDom.render(
   <React.StrictMode>
    <storesContext.Provider value={stores}>
      <App />
    </storesContext.Provider>
    </React.StrictMode>,

  
  document.getElementById('root')
);
});




// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();

