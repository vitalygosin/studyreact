import React from 'react';
import Minmax from './counters/minmax/Minmax';

export default React.memo(function({ id, title, price, rest, cnt, onChange, onRemove }){
	let changed = (cnt) => onChange(id, cnt);
	let remove = () => onRemove(id);
	
	return <tr key={id}>
		<td>{title}</td>
		<td>
			<Minmax max={rest} current={cnt} changed={changed} />
		</td>
		<td>{price}</td>
		<td>{price * cnt}</td>
		<td>
			<button type="button" onClick={remove}>X</button>
		</td>
	</tr>
});