import { makeObservable, observable, computed, action } from 'mobx';

class OrderStore{
	form = {
		name: { label: 'First Name', value: '', pattern: /^[aA-zZ]{2,}$/, valid: false, errorText: 'Min 2 letters' },
		email: { label: 'Email', value: '', pattern: /^.+@.+\..+$/, valid: false, errorText: 'Invalid email' },
		phone: { label: 'Phone', value: '', pattern: /^\d{7,15}$/, valid: false, errorText: 'Numbers only 7-15' }
	}

	get invalid(){
		return Object.values(this.form).some(field => !field.valid);
	}

	change(name, value){
		let field = this.form[name];
		field.value = value;
		field.valid = field.pattern.test(field.value);
	}

	constructor(rootStore){
		this.rootStore = rootStore;

		makeObservable(this, {
			form: observable,
			invalid: computed,
			change: action.bound
		});
	}
}

export default OrderStore;