import { makeObservable, observable, computed, action, runInAction } from 'mobx';

class CartStore{
	products = [];

	get productsDetailed(){
		return this.products.map(pr => ({
			...this.rootStore.products.item(pr.id),
			...pr
		}));
	}

	get total(){
		return this.productsDetailed.reduce((sum, pr) => sum + pr.cnt * pr.price, 0);
	}

	inCart = (id) => {
		return this._findIndex(id) !== -1;
	}

	load(){
		this.api.load(this.token).then(cart => runInAction(() => {
			this.token = cart.token;
			this.products = cart.cart;

			if(cart.needUpdate){
				this.rootStore.storage.setItem('CART_TOKEN', this.token);
			}
		}));
	}

	add(id){
		if(!this.inCart(id)){
			this.api.add(this.token, id).then(res => runInAction(() => {
				if(res){
					this.products.push({ id, cnt: 1 });
				}
			}));
		}
	}

	setCnt(id, cnt){
		let i = this._findIndex(id);

		if(i !== -1){
			this.api.change(this.token, id, cnt).then(res => runInAction(() => {
				if(res){
					this.products[i].cnt = cnt;
				}
			}));
		}
	}

	remove(id){
		let i = this._findIndex(id);

		if(i !== -1){
			this.api.remove(this.token, id).then(res => runInAction(() => {
				if(res){
					this.products.splice(i, 1);
				}
			}));
		}
	}

	_findIndex(id){
		return this.products.findIndex(pr => pr.id == id);
	}

	constructor(rootStore){
		this.rootStore = rootStore;
		this.api = this.rootStore.api.cart;
		this.token = this.rootStore.storage.getItem('CART_TOKEN');

		makeObservable(this, {
			products: observable,
			productsDetailed: computed,
			total: computed,
			load: action.bound,
			add: action.bound,
			setCnt: action.bound,
			remove: action.bound
		});
	}
}

export default CartStore;
