import CartStore from './cart';
import OrderStore from './order';
import ProductsStore from './products';

import * as productsApi from '../api/products';
import * as cartApi from '../api/cart';



class RootStore{
	constructor(){
		this.api = {
			products: productsApi,
			cart: cartApi
		}

		this.storage = window.localStorage;

		this.products = new ProductsStore(this);
		this.order = new OrderStore(this);
		this.cart = new CartStore(this);
	}
}

export default new RootStore();