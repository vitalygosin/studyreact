let baseUrl = 'http://faceprog.ru/reactcourseapi/cart/'

export function load(token){
	return fetch(baseUrl + 'load.php?token=' + token).then(response => response.json());
}

export function add(token, id){
    return fetch(baseUrl + 'add.php?token=' + token +'&id='+id).then(response => response.json());
}

export function remove(token, id){
    return fetch(baseUrl + 'remove.php?token=' + token +'&id='+id).then(response => response.json());
}

export function change(token, id, cnt){
    return fetch(baseUrl + '/change.php?token=' + token +'&id='+id+'&cnt='+cnt).then(response => response.json());
}
export function clean(token){
    return fetch(baseUrl + '/clean.php?token=' + token).then(response => response.json());
}