import React, { useState, useEffect, useContext } from 'react';
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

//import orderStore from './../store/order';

import {Link, Redirect, useHistory} from "react-router-dom";

import { observer } from 'mobx-react-lite';
import storesContext from './../contexts/stores';

export default observer(function({ onPrev, onNext }){
	

	let { order: orderStore } = useContext(storesContext);
	let { form: data, change, invalid } = orderStore;

	let formFields = Object.entries(data).map(([name, field]) => (
		<div className="form-group" key={name}>
			<label>{field.label}</label>
			<input
				type="text" 
				className="form-control"
				value={field.value}
				onChange={e => change(name, e.target.value.trim())}
			/>
			{ field.value !== '' && !field.valid && 
				<div className="small text-danger">{field.errorText}</div>
			}
		</div>
	));

	let [ confirmation, setConfirmation ] = useState(false);
	let showModal = () => setConfirmation(true);
	let hideModal = () => setConfirmation(false);

	let [ confirmed, setConfirmed ] = useState(false);
	let confirm = () => {
		setConfirmed(true);
		hideModal();
	}

	const history = useHistory();
	let onExited = () => {
		if(confirmed){
			history.push('/result')
		}
	}

	return <>
		<h1>Form</h1>
		<hr/>
		{formFields}
		<hr/>
		<button type="button"><Link to='/cart'>Back to cart</Link></button> | 
		<button type="button" onClick={showModal} disabled={invalid}>Send</button>
		<Modal show={confirmation} onHide={hideModal} onExited={onExited}>
			<Modal.Header closeButton>
				<Modal.Title>Check data</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<p>table.</p>
			</Modal.Body>
			<Modal.Footer>
				<Button variant="secondary" onClick={hideModal}>Back</Button>
				<Button variant="primary" onClick={confirm}>Confirm</Button>
			</Modal.Footer>
		</Modal>
	</>
});

/*
	function Modal(){
		return jsx;
	}

	Modal.Header = function(){
		return jsx;
	}

*/