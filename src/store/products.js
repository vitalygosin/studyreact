import { makeObservable, observable, computed, action, runInAction } from 'mobx';
class ProductsStore{
	items = [];

	get item(){
		return id => this.items.find(pr => pr.id == id);
	}

	load(){
		return this.api.all().then(items => runInAction(() => {
			this.items = items;
		}));
	}

	constructor(rootStore){
		this.rootStore = rootStore;
		this.api = this.rootStore.api.products;

		makeObservable(this, {
			items: observable,
			item: computed,
			load: action
		});
	}
}

export default ProductsStore;