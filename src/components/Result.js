import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import storesContext from './../contexts/stores';

export function Cmp(){
	let { cart: cartStore, order: orderStore } = useContext(storesContext);

	return <>
		<h1>Result for {orderStore.form.name.value}</h1>
		<hr/>
		<div>
			<div><strong>InCart: { cartStore.products.length }</strong></div>
			<div><strong>Total: { cartStore.total }</strong></div>
		</div>
	</>
}

export default observer(Cmp);