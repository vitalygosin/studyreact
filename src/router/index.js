
import Cart from '../components/Cart';
import Products from '../components/Products';
import ProductItem from '../components/Product';
import Order from '../components/Order';
import Result from '../components/Result';
import E404 from '../components/E404';

let routes = [
	{
		name: 'home',
		path: '/',
		component: Products
	},
	{
		name: 'product',
		path: '/product/:id',
		component: ProductItem
	},
	{
		name: 'cart',
		path: '/cart',
		component: Cart
	},
	{
		name: 'order',
		path: '/checkout',
		component: Order
	},
	{
		name: 'result',
		path: '/result',
		component: Result
	},
	{
		path: '**',
		component: E404
	}
];

let routesMap = {};

routes.forEach(r => {
	if(r.hasOwnProperty('name')){
		routesMap[r.name] = r.path;
	}
})

let createPath = (name, params) => {
	if(!routesMap.hasOwnProperty(name)){
		return ''; // throw new Error
	}

	let path = routesMap[name];

	for(let key in params){
		path = path.replace(`:${key}`, params[key]);
	}
	
	return path;
}

export { routesMap, createPath };
export default routes;