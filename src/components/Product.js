import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { useParams } from 'react-router-dom';
import storesContext from '../contexts/stores';
import E404 from './E404';

export default observer(function(){
	let { id } = useParams();
	let { products: productsStore } = useContext(storesContext);
	let product = productsStore.item(id);
	
	if(product === undefined){
		return <E404/>;
	}

	return <div>
		<h1>{ product.title }</h1>
		<hr/>
		btn by { product.id }
	</div>
});